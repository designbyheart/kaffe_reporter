//
//  FirstViewController.h
//  kaffe-reporter
//
//  Created by Predrag Jevtic on 3/25/12.
//  Copyright (c) 2012 Limundo/Design By Heart. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController

-(void)test;

@end
