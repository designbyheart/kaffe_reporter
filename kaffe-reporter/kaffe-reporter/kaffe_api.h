//
//  kaffe_api.h
//  kaffe-reporter
//
//  Created by Predrag Jevtic on 3/25/12.
//  Copyright (c) 2012 Limundo/Design By Heart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPClient.h"

@interface kaffe_api : AFHTTPClient

+ (id)sharedInstance;
-(id)initWithBaseUrl:(NSURL*)url;
- (void)loadSetup;

-(NSDictionary*)data:(NSString*)json;

@end