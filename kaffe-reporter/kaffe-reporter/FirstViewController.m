//
//  FirstViewController.m
//  kaffe-reporter
//
//  Created by Predrag Jevtic on 3/25/12.
//  Copyright (c) 2012 Limundo/Design By Heart. All rights reserved.
//

#import "FirstViewController.h"
#import "kaffe_api.h"
#import "MBProgressHUD.h"
@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[kaffe_api sharedInstance] loadSetup];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(test) name:@"test" object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
-(void)test{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSLog(@"notification is done");
}
@end
