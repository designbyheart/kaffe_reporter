//
//  kaffe_api.m
//  kaffe-reporter
//
//  Created by Predrag Jevtic on 3/25/12.
//  Copyright (c) 2012 Limundo/Design By Heart. All rights reserved.
//

#import "kaffe_api.h"
#import "JSONKit.h"
#import "AFJSONRequestOperation.h"

//#define KaffeAPIBaseUrlString @"http://localhost:8888/api/"
#define KaffeAPIBaseUrlString @"http://kaffe1668.com/api/"
#define KaffeAPIUserName @"test"
#define KaffeAPIUserPass @"test"

@implementation kaffe_api

+(kaffe_api*)sharedInstance{
static kaffe_api *__sharedInstance;
static dispatch_once_t onceToken;
dispatch_once(&onceToken, ^{
    __sharedInstance = [[kaffe_api alloc] initWithBaseUrl:[NSURL URLWithString:KaffeAPIBaseUrlString]];
});
return __sharedInstance;
}
-(id)initWithBaseUrl:(NSURL*)url{
self = [super initWithBaseURL:url];
if (self) {
    //custom settings
    [self setAuthorizationHeaderWithUsername:KaffeAPIUserName password:KaffeAPIUserPass];
    
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
}

return self;
}

-(NSDictionary*)data:(NSString*)json{
    NSData* data=[json dataUsingEncoding:NSJSONReadingAllowFragments];
    return [data objectFromJSONData];
}
- (void)loadSetup {
    [self getPath:@"basesetup/all" parameters:nil 
                                success:^(AFHTTPRequestOperation* operation, id response){
                                    NSLog(@"response %@", [response objectFromJSONData]);
                                   [[NSNotificationCenter defaultCenter] postNotificationName:@"test" object:nil];
                                }
                                failure:^(AFHTTPRequestOperation* operation, NSError *error){
                                    NSLog(@"Fetching errors %@", error);
                                }];
    
}

@end

