//
//  main.m
//  kaffe-reporter
//
//  Created by Predrag Jevtic on 3/25/12.
//  Copyright (c) 2012 Limundo/Design By Heart. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
